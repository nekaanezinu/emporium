ActiveAdmin.register Book do

  filter :name
  filter :category_id, as: :search_select_filter
  # filter :author_ids, as: :search_select_filter
  filter :release_date, as: :date_range

  index do
    selectable_column
    id_column
    column :name
    column :release_date
    column :category
    column :authors do |book|
      link_to "#{book.authors.first.name}", [:admin, book.authors.first]
    end
    column "Picture" do |book|
      image_tag book.picture.url, class: 'admin-list-image'
    end
    actions
  end

  permit_params :name, :description, :release_date, :category_id, :author_ids, :picture

  form do |f|
    f.inputs do
      f.input :name
      f.input :description
      f.input :release_date, as: :date_time_picker
      f.input :category
      f.input :author_ids, as: :selected_list
      f.input :picture
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :description
      row :release_date
      row :category
      table_for book.authors do
        column 'Authors' do |author|
          link_to author.name, [:admin, author]
        end
      end
      row :picture do |book|
        image_tag book.picture.url
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end
end
