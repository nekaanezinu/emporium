ActiveAdmin.register User do
  permit_params :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions defaults: true do |user|
      link_to fa_icon("eye"), impersonate_admin_user_path(user), class: "impersonate_link member_link"
    end
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

  member_action :impersonate do
    impersonate_user(User.find(params[:id]))
    redirect_to root_path
  end
end
