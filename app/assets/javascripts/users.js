var UsersController = Paloma.controller('Users');

UsersController.prototype.edit = function(){
  $('[type="file"]').on('change', function() {
    readFile($('[type="file"]').prop('files')[0]).then(res => {
      $('#avatar-preview').attr('src', res);
    })
  })
};

function readFile(file){
  var max_size = 2;
  var max_size_bytes = max_size * 1048576;
  return new Promise((resolve, reject) => {
      if (file.size > max_size_bytes) {
          console.log("file is too big at " + (file.size / 1048576) + "MB");
          reject("file exceeds max size of " + max_size + "MB");
      }
      else {
      var fr = new FileReader();  
      fr.onloadend = () => {
          data = fr.result;
          resolve(data)
      };
      fr.readAsDataURL(file);
      }
  });
}