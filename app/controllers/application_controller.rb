class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale
  impersonates :user

  def access_denied(param)
    redirect_to root_path unless current_user.try(:admin?)
  end

  def set_locale
    I18n.locale = user_signed_in? ? current_user.language.to_sym : I18n.default_locale
  end
end
