class BooksController < ApplicationController

  def index; end

  def search
    @books = Book.search(params[:search], fields: [:name], misspellings: {below: 10}, includes: [:authors]).results
  end

  def show
    @book = Book.find(params[:id])
    @book.click = 0 unless @book.click.present?
    count = @book.click + 1
    @book.update_attributes(click: count)
  end

  def add_to_cart
    book = Book.find(params[:id])
    if !current_user.cart.present?
      cart = Cart.new
      cart.user = current_user
      cart.save!
    end
    CartActions::AddBookToCart.new(current_user.cart, book).call
    respond_to do |format|
      format.html { redirect_to request.referrer, notice: "#{book.name} was added to the cart" }
    end
  end

  def remove_from_cart
    book = Book.find(params[:id])
    text = ''
    if params[:all]
      CartActions::RemoveBookFromCart.new(current_user.cart, book, true).call
      text = "All #{book.name} were removed from the cart"
    else
      CartActions::RemoveBookFromCart.new(current_user.cart, book).call
      text = "#{book.name} was removed from the cart"
    end
    respond_to do |format|
      format.html { redirect_to request.referrer, notice: text }
    end
  end

  def most_popular
    @books = Book.all.order(:click).page params[:page]
  end

  def highest_rated
    #! ass performance, this is like REALLY bad
    @books = Kaminari.paginate_array(Book.all.select(&:has_rates?).first(30).sort_by { |b| -b.stars_average.avg }).page params[:page]
  end

  def most_recent
    @books = Book.all.order(:created_at).page params[:page]
  end
end
