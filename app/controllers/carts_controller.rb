class CartsController < ApplicationController
  def index
    @cart = current_user.cart
    respond_to do |format|
      format.html
    end
  end
end
