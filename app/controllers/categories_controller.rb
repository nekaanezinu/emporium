class CategoriesController < ApplicationController
  def index
    @categories = Category.all.page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: @categories }
    end
  end

  def show
    @category = Category.find params[:id]
    @books = @category.books.page params[:page]
  end
end
