class CommentsController < ApplicationController

  def create
    @comment = Comment.new(comment_params)
    @comment.book_id = params[:book_id]
    @comment.user = current_user
    respond_to do |format|
      if @comment.save!
        format.html { redirect_to request.referrer, notice: 'Comment was successfully created.' }
      else
        format.html { redirect_to request.referrer, error: 'Something went wrong.' }
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    if @comment.user == current_user
      @comment.destroy
      respond_to do |format|
        format.html { redirect_to request.referrer, notice: 'Comment was successfully destroyed.' }
      end
    else
      respond_to do |format|
        format.html { redirect_to request.referrer, error: 'You can only delete your own comments, bruh.' }
      end
    end
  end

  private

  def comment_params
    params
      .require(:comment)
      .permit(:message, :book_id)
  end
end
