class HomeController < ApplicationController
  def index
    @most_popular = Book.includes(:authors).first(4)
    @most_recent = Book.includes(:authors).offset(4).first(4)
    @highest_rated = Book.includes(:authors).last(4)
  end
end
