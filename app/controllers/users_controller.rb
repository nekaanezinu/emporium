class UsersController < ApplicationController
  before_action :set_user

  def show
  end

  def edit
  end

  def update
    if params[:user].present? && params[:user][:password].blank? && params[:user][:password_confirmation]
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
    if @user.update_without_password(user_params)
      respond_to do |format|
        format.html { redirect_to user_path(@user), notice: "User information has been saved." }
      end
    else
      @user.validate
      render template: "users/edit", alert: "User information has not been saved. Fix the following issues."
      # respond_to do |format|
      #   format.html { redirect_to request.referrer, alert: "User information has not been saved. Fix the following issues." }
      # end
    end
  end

  def stop_impersonating
    stop_impersonating_user
    redirect_to admin_root_path
  end

  private

  def user_params
    params.require(:user).permit(:email, :name, :password, :password_confirmation, :avatar)
  end

  def set_user
    @user = current_user
  end
end
