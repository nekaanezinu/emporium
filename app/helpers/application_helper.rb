module ApplicationHelper
  def title(page_title)
    content_for(:title) { page_title }
  end

  def meta_description(page_meta_description)
    content_for(:meta_description) { page_meta_description }
  end

  def heading(page_heading)
    content_for(:heading) { page_heading }
  end

  def humanize_boolean(input)
    input ||= ''
    case input.to_s.downcase
    when 't', 'true'
      'Yes'
    else
      'No'
    end
  end

  def css_for_boolean(input)
    if input
      'success'
    else
      'danger'
    end
  end

  def comment_button_text(count)
    if count == 0
      'No Comments'
    elsif count == 1
      '1 Comment'
    else
      "#{count} Comments"
    end
  end

  def flash_key(key)
    if key == 'alert'
      'danger'
    elsif key == 'notice'
      'primary'
    else
      key
    end
  end

  def avatar_url(avatar)
    if avatar.present?
      avatar.url
    else
      'http://via.placeholder.com/200x200'
    end
  end
end
