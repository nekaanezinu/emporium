class Book < ApplicationRecord
  ratyrate_rateable 'stars'

  mount_uploader :picture, PictureUploader

  searchkick searchable: [:name]
  
  belongs_to :category
  has_and_belongs_to_many :authors
  has_many :comments

  serialize :authors

  validates :name, presence: true

  def has_rates?
    self.stars_rates.present?
  end
end
