class Cart < ApplicationRecord
  alias_attribute :books, :carted_books

  has_many :carted_books
  belongs_to :user

  def has_book?(book)
    self.books.find_by(book_id: book.id).present?
  end

  def total_price
    total = 0
    self.books.each do |entry|
      total += entry.total_price
    end
    total
  end

  def total_amount
    self.books.pluck(:amount).sum
  end
end
