class CartedBook < ApplicationRecord
  belongs_to :book

  def total_price
    self.amount * self.book.price
  end
end
