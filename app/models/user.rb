class User < ApplicationRecord
  rolify
  after_create :assign_default_role, :create_cart
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  ratyrate_rater

  mount_uploader :avatar, AvatarUploader
  has_one :cart
  has_many :books, through: :cart
  has_many :comments

  action_store :like, :comment, counter_cache: true

  validates :avatar, file_size: {less_than: 2.megabytes}
  validates :name, presence: true
  validates :email, presence: true

  def assign_default_role
    self.add_role(:newuser) if self.roles.blank?
  end

  def admin?
    self.has_role? :admin
  end
end
