module CartActions
  class AddBookToCart

    def initialize(cart, book)
      @cart = cart
      @book = book
    end

    def call
      add_book
    end

    def add_book
      carted_book = @cart.books.find_by(book_id: @book.id)
      if carted_book.present?
        carted_book.amount += 1
        carted_book.save!
      else
        carted_book = @cart.books.new
        carted_book.book_id = @book.id
        carted_book.amount = 1
        carted_book.save!
      end
    end

  end
end