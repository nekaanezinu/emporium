module CartActions
  class RemoveBookFromCart

    def initialize(cart, book, remove_all = false)
      @cart = cart
      @book = book
      @remove_all = remove_all
    end

    def call
      if @remove_all
        remove_books
      else
        remove_book
      end
    end

    def remove_books
      carted_book = @cart.books.find_by(book_id: @book.id)
      carted_book.destroy!
    end

    def remove_book
      carted_book = @cart.books.find_by(book_id: @book.id)
      if carted_book.amount > 1
        carted_book.amount -= 1
        carted_book.save!
      else
        carted_book.destroy!
      end
    end

  end
end