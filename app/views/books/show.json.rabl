object :@book

attributes :id, :name, :picture, :release_date, :description
child(:author) { attributes :name, :birth_date }

node :comments do
  partial('comments/list', object: @book.comments)
end