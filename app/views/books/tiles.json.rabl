collection @books

attributes :id, :name, :picture, :release_date, :description
child(:author) { attributes :name }
