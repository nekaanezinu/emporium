collection :@comments

attributes :id, :message, :created_at
child(:user) { attributes :name }