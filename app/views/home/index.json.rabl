object false

node :most_popular do
  partial('books/tiles', object: @most_popular)
end

node :most_recent do
  partial('books/tiles', object: @most_recent)
end

node :highest_rated do
  partial('books/tiles', object: @highest_rated)
end
