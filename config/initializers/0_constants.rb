LANGUAGES = {
  'English' => 'en',
  'Latviešu' => 'lv',
  'Русский' => 'ru'
}.freeze
LOCALES = LANGUAGES.values