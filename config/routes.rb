Rails.application.routes.draw do
  post "/rate" => "rater#create", :as => "rate"
  resources :books do
    collection do
      get :search
      get :most_popular
      get :highest_rated
      get :most_recent
    end

    member do
      get :add_to_cart
      get :remove_from_cart
    end

    resources :comments
  end
  resources :authors
  resources :categories
  namespace :admin do
    devise_for :users, ActiveAdmin::Devise.config
  end
  resources :comments
  resources :carts, only: [:index]
  devise_for :users
  resources :users, only: [:show, :edit, :update] do
    get :stop_impersonating, on: :collection
  end
  ActiveAdmin.routes(self)
  root "home#index"
end
