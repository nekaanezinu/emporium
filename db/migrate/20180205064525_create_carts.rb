class CreateCarts < ActiveRecord::Migration[5.1]
  def change
    create_table :carts do |t|
      t.integer :amount
      t.references :book
      t.references :user
    end
  end
end
