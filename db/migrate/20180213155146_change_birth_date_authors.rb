class ChangeBirthDateAuthors < ActiveRecord::Migration[5.1]
  def change
    remove_column :authors, :birth_year
    add_column :authors, :birth_date, :datetime
  end
end
