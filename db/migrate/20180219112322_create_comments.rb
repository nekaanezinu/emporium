class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.text :message
      t.references :user
      t.references :book
      t.integer :likes_count, default: 0
      
      t.timestamps
    end
  end
end
