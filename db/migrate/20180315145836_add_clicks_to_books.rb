class AddClicksToBooks < ActiveRecord::Migration[5.1]
  def change
    add_column :books, :click, :integer
  end
end
