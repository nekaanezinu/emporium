class CreateCartedBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :carted_books do |t|
      t.references :cart
      t.references :book
      t.integer :amount, default: 1
      
      t.timestamps
    end
  end
end
