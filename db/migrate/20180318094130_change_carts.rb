class ChangeCarts < ActiveRecord::Migration[5.1]
  def up
    remove_column :carts, :book_id
    remove_column :carts, :amount
  end

  def down
    add_reference :carts, :book
    add_column :carts, :amount, :integer
  end
end
