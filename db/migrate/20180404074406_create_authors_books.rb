class CreateAuthorsBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :authors_books do |t|
      t.references :book
      t.references :author
      t.timestamps
    end
  end
end
