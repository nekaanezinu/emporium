class RemoveAuthorFromBooks < ActiveRecord::Migration[5.1]
  def change
    remove_reference :books, :author, foreign_key: true
  end
end
